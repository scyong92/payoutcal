﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using VStar.Backend.General.Utility;

namespace Calculator.Logic
{
    public class EatDataReader
    {
        private Dictionary<int, string> _indexToPrizeName = new Dictionary<int, string>();
        private static EatDataReader _instance;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static EatDataReader GetInstance()
        {
            if (_instance == null)
            {
                _instance = new EatDataReader();
            }

            return _instance;
        }

        public EatDataReader()
        {
            _indexToPrizeName.Add(2, "1ST");
            _indexToPrizeName.Add(3, "2ND");
            _indexToPrizeName.Add(4, "3RD");
            _indexToPrizeName.Add(5, "4th");
            _indexToPrizeName.Add(6, "5th");
            _indexToPrizeName.Add(7, "6th");
            _indexToPrizeName.Add(8, "Special");
            _indexToPrizeName.Add(9, "Consolation");
        }

        public ResultLog ReadAndCalculate(string dir)
        {
            ResultLog result = new ResultLog();

            try
            {
                List<PoolResult> poolResults = PoolResultReader.GetInstance().GetPoolResult();
                string eatDataList = File.ReadAllText(dir);
                string[] poolArr = eatDataList.Split(new char[2]
                {
                '\r',
                '\n'
                }, StringSplitOptions.RemoveEmptyEntries);

                List<BetDetailModel> betModels = new List<BetDetailModel>();
                foreach (var ea in poolArr)
                {
                    string[] dataArr = ea.Split(new char[1] { '~'}, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 2;i< dataArr.Length;i++)
                    {
                        string[] eatArr = dataArr[i].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        BetDetailModel betDetailModel = new BetDetailModel();
                        betDetailModel.Stock = decimal.Parse(eatArr[1]);
                        if (betDetailModel.Stock > 0)
                        {
                            betDetailModel.BetNumber = dataArr[1];
                            betDetailModel.EatType = eatArr[0];
                            betDetailModel.PoolId = int.Parse(dataArr[0]);

                            Dictionary<int, decimal> payRate = new Dictionary<int, decimal>();
                            foreach (var prize in _indexToPrizeName)
                            {
                                decimal prizeRate = decimal.Parse(eatArr[prize.Key]);
                                if (prizeRate > 0)
                                {
                                    payRate.Add(prize.Key - 2, prizeRate);
                                }
                            }
                            betDetailModel.PayRate = payRate;
                            betModels.Add(betDetailModel);
                        }
                    }
                }

                Dictionary<int, List<BetDetailModel>> poolToDetailModels = betModels.GroupBy(data => data.PoolId).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

                List<PayoutModel> payoutModelList = new List<PayoutModel>();

                foreach(var ea in poolToDetailModels)
                {
                    PoolResult poolResult = poolResults.FirstOrDefault(i => i.Pool.Id == ea.Key);
                    
                    foreach(var data in ea.Value)
                    {
                        foreach (var payRate in data.PayRate)
                        {
                            PayoutModel model = new PayoutModel();

                            if (data.EatType.ToLower() == "mgold")
                            {
                                if (!string.IsNullOrEmpty(poolResult.MGold))
                                {
                                    var resultNumber = poolResult.MGold;
                                    bool isMatch = data.BetNumber.Substring(0, resultNumber.Length - payRate.Key) == resultNumber.Substring(0, resultNumber.Length - payRate.Key) ||
                                        data.BetNumber.Substring(payRate.Key) == resultNumber.Substring(payRate.Key);

                                    if (payRate.Key == 4 && isMatch)
                                    {
                                        model.BetNumber = resultNumber;
                                        model.BetAmount = data.Stock;
                                        model.EatTypeName = data.EatType;
                                        model.PoolName = poolResult.Pool.PoolName;
                                        model.Payout = payRate.Value * data.Stock;
                                        model.Prize = _indexToPrizeName[payRate.Key + 2];
                                        model.Rate = payRate.Value;
                                        payoutModelList.Add(model);
                                        break;
                                    }
                                }
                            }
                            else if (data.EatType.ToLower() == "5d")
                            {
                                if (poolResult.FiveD.Count() > 0)
                                {
                                    //5d fourth prize will use substring of first prize
                                    if (payRate.Key >= 3)
                                    {
                                        if (data.BetNumber.Substring(payRate.Key - 3) == poolResult.FiveD.ElementAt(0).Substring(payRate.Key - 3))
                                        {
                                            model.BetNumber = poolResult.FiveD.ElementAt(0);
                                            model.BetAmount = data.Stock;
                                            model.EatTypeName = data.EatType;
                                            model.PoolName = poolResult.Pool.PoolName;
                                            model.Payout = payRate.Value * data.Stock;
                                            model.Prize = _indexToPrizeName[payRate.Key + 2];
                                            model.Rate = payRate.Value;
                                            payoutModelList.Add(model);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (poolResult.FiveD.ElementAt(payRate.Key) == data.BetNumber)
                                        {
                                            model.BetNumber = data.BetNumber;
                                            model.BetAmount = data.Stock;
                                            model.EatTypeName = data.EatType;
                                            model.PoolName = poolResult.Pool.PoolName;
                                            model.Payout = payRate.Value * data.Stock;
                                            model.Prize = _indexToPrizeName[payRate.Key + 2];
                                            model.Rate = payRate.Value;
                                            payoutModelList.Add(model);
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (data.EatType.ToLower() == "6d")
                            {
                                if (!string.IsNullOrEmpty(poolResult.SixD))
                                {
                                    var resultNumber = poolResult.SixD;
                                    bool isMatch = data.BetNumber.Substring(0, resultNumber.Length - payRate.Key) == resultNumber.Substring(0, resultNumber.Length - payRate.Key) ||
                                                data.BetNumber.Substring(payRate.Key) == resultNumber.Substring(payRate.Key);

                                    if (isMatch)
                                    {
                                        model.BetNumber = resultNumber;
                                        model.BetAmount = data.Stock;
                                        model.EatTypeName = data.EatType;
                                        model.PoolName = poolResult.Pool.PoolName;
                                        model.Payout = payRate.Value * data.Stock;
                                        model.Prize = _indexToPrizeName[payRate.Key + 2];
                                        model.Rate = payRate.Value;
                                        payoutModelList.Add(model);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                int digitToTake = 4;
                                bool success = int.TryParse(data.EatType.ToLower()[0].ToString(), out digitToTake);
                                if (success == false)
                                {
                                    digitToTake = 4;
                                }
                                var trimmedBetNumber = data.BetNumber;
                                if (data.EatType.ToLower()[0] == '3')
                                {
                                    trimmedBetNumber = trimmedBetNumber.Substring(Math.Max(trimmedBetNumber.Length - 3, 0));
                                }

                                if ((poolResult.TopPrize.Count > payRate.Key && trimmedBetNumber == poolResult.TopPrize[payRate.Key].Substring(Math.Max(poolResult.TopPrize[payRate.Key].Length - digitToTake, 0))) ||
                                    ((payRate.Key == 6 && poolResult.Special.FirstOrDefault(i => i.Substring(Math.Max(i.Length - digitToTake, 0)) == trimmedBetNumber) != null) ||
                                    (payRate.Key == 7 && poolResult.Consolation.FirstOrDefault(i => i.Substring(Math.Max(i.Length - digitToTake, 0)) == trimmedBetNumber) != null)))
                                {
                                    model.BetNumber = trimmedBetNumber;
                                    model.BetAmount = data.Stock;
                                    model.EatTypeName = data.EatType;
                                    model.PoolName = poolResult.Pool.PoolName;
                                    model.Payout = payRate.Value * data.Stock;
                                    model.Prize = _indexToPrizeName[payRate.Key + 2];
                                    model.Rate = payRate.Value;
                                    payoutModelList.Add(model);
                                }
                            }
                        }
                    }
                }

                result.ReturnObject = payoutModelList;
            }
            catch(Exception ex)
            {
                result.Message = ex.Message;
                result.Code = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }
    }
}
