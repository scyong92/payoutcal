﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic
{
    public class BetDetailModel
    {
        public string EatType { get; set; }
        public decimal Stock { get; set; }
        public int PoolId { get; set; }
        public String BetNumber { get; set; }
        public Dictionary<int, decimal> PayRate { get; set; }
    }
}
