﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic
{
    public class PoolResult
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<string> TopPrize { get; set; }
        public List<string> Special { get; set; }
        public List<string> Consolation { get; set; }
        public List<string> FiveD { get; set; }
        public string SixD { get; set; }
        public string MGold { get; set; }
        public Pool Pool { get; set; }
        //for p8 only currently
        public decimal Payout { get; set; }
        public decimal StockAcquired { get; set; }
        public decimal CommPaid { get; set; }

        public PoolResult()
        {
            TopPrize = new List<string>();
            Special = new List<string>();
            Consolation = new List<string>();
            FiveD = new List<string>();
        }

        public string printPrize(int index)
        {
            int counter = index + 1;
            string text = "";
            switch (counter)
            {
                case 1:
                    text = "1st Prize 首獎";
                    break;
                case 2:
                    text = "2nd Prize 二獎";
                    break;
                case 3:
                    text = "3rd Prize 三獎";
                    break;
            }
            return text;
        }
    }
}
