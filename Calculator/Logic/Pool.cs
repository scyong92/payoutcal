﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic
{
    public class Pool
    {
        public string PoolName { get; set; }
        public string PoolShortCode { get; set; }
        public int PoolNumber { get; set; }
        public int Id { get; set; }
        public string PoolImages { get; set; }
        public bool IsDailyPool { get; set; }
        public bool Has5D { get; set; }
        public bool Has6D { get; set; }
        public bool HasMGold { get; set; }

        public int CompareTo(object obj)
        {
            Pool refPool = (Pool)obj;

            if (this.Id == refPool.Id)
            {
                return 0;
            }
            else if (this.Id >= refPool.Id)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public override bool Equals(object obj)
        {
            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            var lhs = (Pool)obj;

            return this.Id == lhs.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
