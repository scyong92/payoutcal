﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using VStar.Backend.General.Utility;
using HtmlAgilityPack;
using System.Globalization;
using System.Net;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

namespace Calculator.Logic
{
    public class PoolResultReader
    {
        private static PoolResultReader _instance;
        private List<Pool> _pools = new List<Pool>();
        private List<PoolResult> _poolResult = new List<PoolResult>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static PoolResultReader GetInstance()
        {
            if (_instance == null)
            {
                _instance = new PoolResultReader();
            }

            return _instance;
        }

        public List<Pool> GetPools()
        {
            return _pools;
        }

        public ResultLog ReadConfigPool()
        {
            ResultLog result = new ResultLog();
            _pools.Clear();
            try
            {
                string poolData = File.ReadAllText("pool.config");
                string[] poolArr = poolData.Split(new char[2]
                        {
                        '\r',
                        '\n'
                        }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var ea in poolArr)
                {
                    string[] poolDetail = ea.Split(',');
                    Pool pool = new Pool();
                    pool.Has5D = poolDetail[7].ToString() == "1";
                    pool.Has6D = poolDetail[8].ToString() == "1";
                    pool.HasMGold = poolDetail[9].ToString() == "1";
                    pool.IsDailyPool = poolDetail[6].ToString() == "1";
                    pool.Id = int.Parse(poolDetail[0].ToString());
                    pool.PoolName = poolDetail[1].ToString();
                    pool.PoolShortCode = poolDetail[2].ToString();
                    pool.PoolImages = poolDetail[5].ToString();
                    _pools.Add(pool);
                }
                result.ReturnObject = _pools;
            }
            catch(Exception ex)
            {
                result.Code = (int)HttpStatusCode.BadRequest;
                result.Message = ex.Message;
            }

            return result;
        }

        public ResultLog CheckResult(string date)
        {
            ResultLog result = new ResultLog();
            List<PoolResult> model = new List<PoolResult>();
            try
            {
                HtmlWeb web = new HtmlWeb();

                string url = "https://www.check4d.com/";
                DateTime parsedDate = DateTime.Today;

                List<PoolResult> results = new List<PoolResult>();

                bool repeat = true;
                DateTime resultDate = DateTime.Today;

                if (!String.IsNullOrEmpty(date))
                {
                    parsedDate = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    resultDate = parsedDate;
                    var converted_date = parsedDate.ToString("yyyy-MM-dd");

                    if (converted_date != DateTime.Today.ToString("yyyy-MM-dd"))
                    {
                        url += "past-results/" + converted_date;
                        repeat = false;
                    }
                }

                PoolResult poolResult = new PoolResult();

                HtmlDocument doc = web.LoadFromWebAsync(url).Result;
                HtmlNodeCollection dateNode = doc.DocumentNode.SelectNodes("(//*/table[@class='resultTable2'])[2]/tr/td[1]");

                string strDate = dateNode.First().InnerText;
                string trimmedDate = strDate.Substring(strDate.IndexOf(" "), 12).Trim();
                DateTime drawDate = DateTime.ParseExact(trimmedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (drawDate.ToString("dd/MM/yyyy") == date)
                {

                    List<PoolResult> results_west = scrapeResult(url, drawDate, true);
                    if (results_west.Count() > 0)
                    {
                        results.AddRange(results_west);
                    }

                    if (repeat)
                    {
                        List<PoolResult> results_east = scrapeResult(url + "sabah-sarawak-4d-results", drawDate);
                        List<PoolResult> results_sg = scrapeResult(url + "singapore-4d-results", drawDate);
                        if (results_east.Count() > 0)
                        {
                            results.AddRange(results_east);
                        }

                        if (results_sg.Count() > 0)
                        {
                            results.AddRange(results_sg);
                        }
                    }
                }

                _poolResult = results;
                result.ReturnObject = results;
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.BadRequest;
                result.Message = ex.Message;
            }
            return result;
        }

        public List<PoolResult> GetPoolResult()
        {
            return _poolResult;
        }

        public List<PoolResult> scrapeResult(string url, DateTime date, bool retrieveApi = false)
        {
            // Seperate pool into dictionary
            Dictionary<string, Pool> poolDict = new Dictionary<string, Pool>();

            if (_pools.Where(p => p.Id == 1).FirstOrDefault() != null)
            {
                poolDict.Add("Magnum 4D 萬能", _pools.Where(p => p.Id == 1).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 2).FirstOrDefault() != null)
            {
                poolDict.Add("Da Ma Cai 1+3D 大馬彩", _pools.Where(p => p.Id == 2).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 3).FirstOrDefault() != null)
            {
                poolDict.Add("SportsToto 4D 多多", _pools.Where(p => p.Id == 3).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 3).FirstOrDefault() != null)
            {
                poolDict.Add("SportsToto 5D, 6D, Lotto多多六合彩", _pools.Where(p => p.Id == 3).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 6).FirstOrDefault() != null)
            {
                poolDict.Add("Sandakan 4D山打根赛马会", _pools.Where(p => p.Id == 6).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 7).FirstOrDefault() != null)
            {
                poolDict.Add("Special CashSweep 砂勞越大萬", _pools.Where(p => p.Id == 7).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 15).FirstOrDefault() != null)
            {
                poolDict.Add("Sabah 88 4D 沙巴萬字", _pools.Where(p => p.Id == 5).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 4).FirstOrDefault() != null)
            {
                poolDict.Add("Singapore 4D", _pools.Where(p => p.Id == 4).FirstOrDefault());
            }

            Dictionary<string, Pool> poolDict2 = new Dictionary<string, Pool>();

            if (_pools.Where(p => p.Id == 8).FirstOrDefault() != null)
            {
                poolDict2.Add("GDLotto", _pools.Where(p => p.Id == 8).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 10).FirstOrDefault() != null)
            {
                poolDict2.Add("Perdana4D", _pools.Where(p => p.Id == 510).FirstOrDefault());
            }

            if (_pools.Where(p => p.Id == 11).FirstOrDefault() != null)
            {
                poolDict2.Add("Harihari4D", _pools.Where(p => p.Id == 11).FirstOrDefault());
            }

            List<PoolResult> results = new List<PoolResult>();
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.LoadFromWebAsync(url).Result;
            HtmlNodeCollection poolTables = doc.DocumentNode.SelectNodes("//div[@class='outerbox']");

            if (poolTables != null)
            {
                foreach (HtmlNode poolTable in poolTables)
                {
                    PoolResult newResult = new PoolResult();
                    newResult.Date = date;
                    bool proceed = false;

                    foreach (HtmlNode item in poolTable.SelectNodes(".//table[@class='resultTable2']/tr/td"))
                    {
                        var poolName = item.InnerText;

                        //Found correct pool
                        if (poolDict.ContainsKey(poolName))
                        {
                            if (results.FirstOrDefault(i=>i.Pool.PoolName == poolDict[poolName].PoolName) != null)
                            {
                                newResult = results.FirstOrDefault(i => i.Pool.PoolName == poolDict[poolName].PoolName);
                            }
                            else
                            {
                                newResult.Pool = poolDict[poolName];
                                newResult.TopPrize = new List<string>();
                                newResult.Special = new List<string>();
                                newResult.Consolation = new List<string>();
                                newResult.FiveD = new List<string>();

                                results.Add(newResult);
                            }
                            proceed = true;
                            break;
                        }
                    }

                    if (proceed)
                    {
                        //Scrape: top prize
                        HtmlNodeCollection topPrize = poolTable.SelectNodes(".//td[@class='resulttop']");

                        if (topPrize != null)
                        {
                            foreach (HtmlNode item in topPrize)
                            {
                                if (!String.IsNullOrEmpty(item.InnerText) || item.InnerText == "&nbsp;")
                                {
                                    newResult.TopPrize.Add(item.InnerText);
                                    if (newResult.TopPrize.Count == 3)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                        }

                        bool is5D6D = false;
                        //Scrape: special, consolation, 5D, 6D
                        foreach (HtmlNode special_consolationPrize in poolTable.SelectNodes(".//td[@class='resultbottom']"))
                        {
                            string number = special_consolationPrize.InnerText;
                            
                            if (number != "")
                            {
                                if (number.Length == 5 && newResult.FiveD.Count < 3 && number != "&nbsp;")
                                {
                                    is5D6D = true;
                                    newResult.FiveD.Add(number);
                                }
                                else if (number.Length == 6 && number != "&nbsp;")
                                {
                                    is5D6D = true;
                                    newResult.SixD = number;
                                    break;
                                }
                                else if (!is5D6D)
                                {
                                    string[] thirteenBoxes = { "MAGNUM", "TOTO", "SARAWAK", "SABAH", "STC" };
                                    // Magnum, Toto, STC, Sabah has 13 boxes
                                    if (
                                        (newResult.Special.Count < 13 && Array.IndexOf(thirteenBoxes, newResult.Pool.PoolName) >= 0) ||
                                        (newResult.Special.Count < 10 && Array.IndexOf(thirteenBoxes, newResult.Pool.PoolName) < 0)
                                        )
                                    {
                                        if (number == "&nbsp;" || number == "-")
                                        {
                                            number = "----";
                                        }
                                        newResult.Special.Add(number);
                                    }
                                    else
                                    {
                                        newResult.Consolation.Add(number);
                                    }
                                }
                            }
                        }

                        newResult.Pool = new Pool()
                        {
                            Id = newResult.Pool.Id,
                            PoolImages = newResult.Pool.PoolImages,
                            PoolName = newResult.Pool.PoolName,
                            Has5D = (newResult.FiveD.Count > 0),
                            Has6D = !string.IsNullOrEmpty(newResult.SixD),
                            HasMGold = newResult.Pool.HasMGold
                        };

                        if (newResult.TopPrize.Count != 3)
                        {
                            int currentCount = newResult.TopPrize.Count;
                            for (int i = 0; i < 3 - currentCount; i++)
                            {
                                newResult.TopPrize.Add("----");
                            }
                        }

                        if (newResult.Special.Count != 13)
                        {
                            int currentCount = newResult.Special.Count;
                            for (int i = 0; i < 13 - currentCount; i++)
                            {
                                newResult.Special.Add("----");
                            }
                        }

                        if (newResult.Consolation.Count != 10)
                        {
                            int currentCount = newResult.Consolation.Count;
                            for (int i = 0; i < 10 - currentCount; i++)
                            {
                                newResult.Consolation.Add("----");
                            }
                        }
                    }
                }
            }

            if (retrieveApi)
            {
                try
                {
                    String parsedDate = date.ToString("yyyy-MM-dd");

                    // Scrape result for grand dragon (H), perdana (N), hari hari (R)
                    var otrUrl = "https://4dyes.com/getLiveResult.php?date=" + parsedDate;

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(otrUrl);

                        HttpResponseMessage response = client.GetAsync(otrUrl).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            string jsonStr = response.Content.ReadAsStringAsync().Result;
                            var json = JsonConvert.DeserializeObject<ResultObjJSON>(jsonStr);

                            foreach (KeyValuePair<String, Pool> pool in poolDict2)
                            {
                                ResultJSON obj = new ResultJSON();
                                switch (pool.Key)
                                {
                                    case "GDLotto":
                                        obj = json.H;
                                        break;
                                    case "Perdana4D":
                                        obj = json.N;
                                        break;
                                    case "Harihari4D":
                                        obj = json.R;
                                        break;
                                }
                                results.Add(ProcessResultFromAPI(date, obj, pool.Value));
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
            }

            return results;
        }

        private PoolResult ProcessResultFromAPI(DateTime date, ResultJSON resultObj, Pool selectedPool)
        {
            try
            {
                PoolResult result = new PoolResult();
                result.Date = date;
                result.Pool = selectedPool;

                PoolResult resultService = new PoolResult();

                result.TopPrize.AddRange(new List<String>{
                    resultObj._1.First(),
                    resultObj._2.First(),
                    resultObj._3.First()
                });

                result.Special = resultObj._P.ToList();
                result.Consolation = resultObj.C;

                if (selectedPool.Has5D)
                {
                    result.FiveD.AddRange(new List<String>
                    {
                        resultObj._5D1.First(),
                        resultObj._5D2.First(),
                        resultObj._5D3.First(),
                    });

                    result.SixD = resultObj._6D.First();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class ResultJSON
    {
        public String DrawDate { get; set; }
        public String Name { get; set; }
        public List<String> _1 { get; set; }
        public List<String> _2 { get; set; }
        public List<String> _3 { get; set; }
        public List<String> _P { get; set; }
        public List<String> C { get; set; }
        public List<String> _1pos { get; set; }
        public List<String> _2pos { get; set; }
        public List<String> _3pos { get; set; }
        public List<String> _5D1 { get; set; }
        public List<String> _5D2 { get; set; }
        public List<String> _5D3 { get; set; }
        public List<String> _6D { get; set; }
    }

    public class ResultObjJSON
    {
        public ResultJSON M { get; set; }
        public ResultJSON P { get; set; }
        public ResultJSON T { get; set; }
        public ResultJSON S { get; set; }
        public ResultJSON B { get; set; }
        public ResultJSON K { get; set; }
        public ResultJSON W { get; set; }
        public ResultJSON N { get; set; }
        public ResultJSON H { get; set; }
        public ResultJSON V { get; set; }
        public ResultJSON R { get; set; }
        public ResultJSON _5D { get; set; }
        public ResultJSON _6D { get; set; }
    }
}
