﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic
{
    public class PayoutModel
    {
        public string PoolName { get; set; }
        public string BetNumber { get; set; }
        public string Prize { get; set; }
        public string EatTypeName { get; set; }
        public decimal BetAmount { get; set; }
        public decimal Rate { get; set; }
        public decimal Payout { get; set; }
    }
}
