﻿using Calculator.Logic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator.Gui
{
    public class PoolResultPanel4D : TabPage
    {
        private PoolResult _poolResult;
        private AnchorStyles _labelStyle = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom);
        private TableLayoutPanel _mainPanel = new TableLayoutPanel();
        private Label _dummyLabel = new Label();

        public PoolResultPanel4D(PoolResult poolResult)
        {
            _poolResult = poolResult;

            Init();
        }

        private void Init()
        {
            _dummyLabel.Text = "----";

            Label firstPrizeLabel = new Label();
            firstPrizeLabel.Text = "First Prize";
            firstPrizeLabel.BackColor = Color.Transparent;
            firstPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            firstPrizeLabel.Anchor = _labelStyle;

            Label secondPrizeLabel = new Label();
            secondPrizeLabel.Text = "Second Prize";
            secondPrizeLabel.BackColor = Color.Transparent;
            secondPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            secondPrizeLabel.Anchor = _labelStyle;

            Label thirdPrizeLabel = new Label();
            thirdPrizeLabel.Text = "Third Prize";
            thirdPrizeLabel.BackColor = Color.Transparent;
            thirdPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            thirdPrizeLabel.Anchor = _labelStyle;

            Label firstPrizeVal = new Label();
            firstPrizeVal.Text = _poolResult.TopPrize[0];
            firstPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            firstPrizeVal.Anchor = _labelStyle;

            Label secondPrizeVal = new Label();
            secondPrizeVal.Text = _poolResult.TopPrize[1];
            secondPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            secondPrizeVal.Anchor = _labelStyle;

            Label thirdPrizeVal = new Label();
            thirdPrizeVal.Text = _poolResult.TopPrize[2];
            thirdPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            thirdPrizeVal.Anchor = _labelStyle;


            TableLayoutPanel topPrizeTable = new TableLayoutPanel();
            topPrizeTable.CellPaint += TableFirstColPaint;
            topPrizeTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            topPrizeTable.Dock = DockStyle.Fill;
            topPrizeTable.ColumnCount = 2;
            topPrizeTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            topPrizeTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            topPrizeTable.RowCount = 3;
            topPrizeTable.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            topPrizeTable.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            topPrizeTable.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            topPrizeTable.Controls.Add(firstPrizeLabel, 0, 0);
            topPrizeTable.Controls.Add(secondPrizeLabel, 0, 1);
            topPrizeTable.Controls.Add(thirdPrizeLabel, 0, 2);

            topPrizeTable.Controls.Add(firstPrizeVal, 1, 0);
            topPrizeTable.Controls.Add(secondPrizeVal, 1, 1);
            topPrizeTable.Controls.Add(thirdPrizeVal, 1, 2);

            Label titleSpecial = new Label();
            titleSpecial.Text = "Special";
            titleSpecial.TextAlign = ContentAlignment.MiddleCenter;
            titleSpecial.Anchor = _labelStyle;
            titleSpecial.BackColor = Color.Transparent;

            TableLayoutPanel specialPrize = new TableLayoutPanel();
            specialPrize.CellPaint += TableFirstRowPaint;
            specialPrize.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            specialPrize.Dock = DockStyle.Fill;
            specialPrize.ColumnCount = 5;
            specialPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            specialPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            specialPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            specialPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            specialPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            specialPrize.RowCount = 4;
            specialPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            specialPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            specialPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            specialPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            specialPrize.Controls.Add(titleSpecial,0,0);
            specialPrize.SetColumnSpan(titleSpecial, 5);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Label dummy = new Label();
                    dummy.Anchor = _labelStyle;
                    if (i == 2 && (j == 0 || j == 4))
                    {
                        dummy.Text = "";
                    }
                    else
                    {
                        int jVal = j;
                        if (i == 2)
                        {
                            jVal--;
                        }
                        dummy.Text = _poolResult.Special[(i * 5) + jVal];
                    }
                    dummy.TextAlign = ContentAlignment.MiddleCenter;
                    specialPrize.Controls.Add(dummy, j, i + 1);
                }
            }


            Label titleConsolation = new Label();
            titleConsolation.Text = "Consolation";
            titleConsolation.TextAlign = ContentAlignment.MiddleCenter;
            titleConsolation.Anchor = _labelStyle;
            titleConsolation.BackColor = Color.Transparent;

            TableLayoutPanel consolationPrize = new TableLayoutPanel();
            consolationPrize.CellPaint += TableFirstRowPaint;
            consolationPrize.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            consolationPrize.Dock = DockStyle.Fill;
            consolationPrize.ColumnCount = 5;
            consolationPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            consolationPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            consolationPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            consolationPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            consolationPrize.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            consolationPrize.RowCount = 3;
            consolationPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 30F));
            consolationPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 35F));
            consolationPrize.RowStyles.Add(new RowStyle(SizeType.Percent, 35F));
            consolationPrize.Controls.Add(titleConsolation, 0, 0);
            consolationPrize.SetColumnSpan(titleConsolation, 5);

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Label dummy = new Label();
                    dummy.Anchor = _labelStyle;
                    dummy.TextAlign = ContentAlignment.MiddleCenter;
                    dummy.Text = _poolResult.Consolation[(i * 5) + j];
                    consolationPrize.Controls.Add(dummy, j, i+ 1);
                }
            }

            TableLayoutPanel _mainPanel = new TableLayoutPanel();
            _mainPanel.ColumnCount = 1;
            _mainPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            _mainPanel.RowCount = 3;
            _mainPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            _mainPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            _mainPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33F));
            _mainPanel.Dock = DockStyle.Fill;

            _mainPanel.Controls.Add(topPrizeTable);
            _mainPanel.Controls.Add(specialPrize);
            _mainPanel.Controls.Add(consolationPrize);
            Controls.Add(_mainPanel);
        }

        private void TableFirstColPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Column == 0)
            {
                e.Graphics.FillRectangle(Brushes.YellowGreen, e.CellBounds);
            }
        }

        private void TableFirstRowPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row == 0)
            {
                e.Graphics.FillRectangle(Brushes.YellowGreen, e.CellBounds);
            }
        }
    }
}
