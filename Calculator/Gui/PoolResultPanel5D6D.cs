﻿using Calculator.Logic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator.Gui
{
    public class PoolResultPanel5D6D : TabPage
    {
        private PoolResult _poolResult;
        private AnchorStyles _labelStyle = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom);

        private TableLayoutPanel _mainPanel = new TableLayoutPanel();

        public PoolResultPanel5D6D(PoolResult poolResult)
        {
            _poolResult = poolResult;

            Init();
        }

        private void Init()
        {
            Label title5D = new Label();
            title5D.Text = "5D";
            title5D.TextAlign = ContentAlignment.MiddleCenter;
            title5D.Anchor = _labelStyle;
            title5D.BackColor = Color.Transparent;

            Label orLabel = new Label();
            orLabel.Text = "or";
            orLabel.TextAlign = ContentAlignment.MiddleCenter;
            orLabel.Anchor = _labelStyle;

            Label firstPrizeLabel = new Label();
            firstPrizeLabel.Text = "1st Prize";
            firstPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            firstPrizeLabel.Anchor = _labelStyle;
            firstPrizeLabel.BackColor = Color.Transparent;

            Label secondPrizeLabel = new Label();
            secondPrizeLabel.Text = "2nd Prize";
            secondPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            secondPrizeLabel.Anchor = _labelStyle;
            secondPrizeLabel.BackColor = Color.Transparent;

            Label thirdPrizeLabel = new Label();
            thirdPrizeLabel.Text = "3rd Prize";
            thirdPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            thirdPrizeLabel.Anchor = _labelStyle;
            thirdPrizeLabel.BackColor = Color.Transparent;

            Label fourthPrizeLabel = new Label();
            fourthPrizeLabel.Text = "4th Prize";
            fourthPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            fourthPrizeLabel.Anchor = _labelStyle;
            fourthPrizeLabel.BackColor = Color.Transparent;

            Label fifthPrizeLabel = new Label();
            fifthPrizeLabel.Text = "5th Prize";
            fifthPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            fifthPrizeLabel.Anchor = _labelStyle;
            fifthPrizeLabel.BackColor = Color.Transparent;

            Label sixthPrizeLabel = new Label();
            sixthPrizeLabel.Text = "6th Prize";
            sixthPrizeLabel.TextAlign = ContentAlignment.MiddleCenter;
            sixthPrizeLabel.Anchor = _labelStyle;
            sixthPrizeLabel.BackColor = Color.Transparent;

            Label firstPrizeVal = new Label();
            firstPrizeVal.Text = _poolResult.FiveD[0];
            firstPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            firstPrizeVal.Anchor = _labelStyle;

            Label secondPrizeVal = new Label();
            secondPrizeVal.Text = _poolResult.FiveD[1];
            secondPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            secondPrizeVal.Anchor = _labelStyle;

            Label thirdPrizeVal = new Label();
            thirdPrizeVal.Text = _poolResult.FiveD[2];
            thirdPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            thirdPrizeVal.Anchor = _labelStyle;

            Label fourthPrizeVal = new Label();
            fourthPrizeVal.Text = _poolResult.FiveD[0].Substring(1, 4);
            fourthPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fourthPrizeVal.Anchor = _labelStyle;

            Label fifthPrizeVal = new Label();
            fifthPrizeVal.Text = _poolResult.FiveD[1].Substring(2, 3);
            fifthPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fifthPrizeVal.Anchor = _labelStyle;

            Label sixthPrizeVal = new Label();
            sixthPrizeVal.Text = _poolResult.FiveD[2].Substring(3, 2);
            sixthPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            sixthPrizeVal.Anchor = _labelStyle;

            TableLayoutPanel table5D = new TableLayoutPanel();
            table5D.CellPaint += Table5DPaint;
            table5D.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            table5D.Dock = DockStyle.Fill;
            table5D.ColumnCount = 4;
            table5D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table5D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table5D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table5D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table5D.RowCount = 3;
            table5D.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            table5D.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            table5D.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            table5D.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));

            table5D.Controls.Add(title5D);
            table5D.SetColumnSpan(title5D, 4);

            table5D.Controls.Add(firstPrizeLabel, 0, 1);
            table5D.Controls.Add(secondPrizeLabel, 0, 2);
            table5D.Controls.Add(thirdPrizeLabel, 0, 3);
            table5D.Controls.Add(fourthPrizeLabel, 2, 1);
            table5D.Controls.Add(fifthPrizeLabel, 2, 2);
            table5D.Controls.Add(sixthPrizeLabel, 2, 3);

            table5D.Controls.Add(firstPrizeVal, 1, 1);
            table5D.Controls.Add(secondPrizeVal, 1, 2);
            table5D.Controls.Add(thirdPrizeVal, 1, 3);
            table5D.Controls.Add(fourthPrizeVal, 3, 1);
            table5D.Controls.Add(fifthPrizeVal, 3, 2);
            table5D.Controls.Add(sixthPrizeVal, 3, 3);

            //6d
            Label firstPrize6DLabel = new Label();
            firstPrize6DLabel.Text = "1st Prize";
            firstPrize6DLabel.TextAlign = ContentAlignment.MiddleCenter;
            firstPrize6DLabel.Anchor = _labelStyle;
            firstPrize6DLabel.BackColor = Color.Transparent;

            Label secondPrize6DLabel = new Label();
            secondPrize6DLabel.Text = "2nd Prize";
            secondPrize6DLabel.TextAlign = ContentAlignment.MiddleCenter;
            secondPrize6DLabel.Anchor = _labelStyle;
            secondPrize6DLabel.BackColor = Color.Transparent;

            Label thirdPrize6DLabel = new Label();
            thirdPrize6DLabel.Text = "3rd Prize";
            thirdPrize6DLabel.TextAlign = ContentAlignment.MiddleCenter;
            thirdPrize6DLabel.Anchor = _labelStyle;
            thirdPrize6DLabel.BackColor = Color.Transparent;

            Label fourthPrize6DLabel = new Label();
            fourthPrize6DLabel.Text = "4th Prize";
            fourthPrize6DLabel.TextAlign = ContentAlignment.MiddleCenter;
            fourthPrize6DLabel.Anchor = _labelStyle;
            fourthPrize6DLabel.BackColor = Color.Transparent;

            Label fifthPrize6DLabel = new Label();
            fifthPrize6DLabel.Text = "5th Prize";
            fifthPrize6DLabel.TextAlign = ContentAlignment.MiddleCenter;
            fifthPrize6DLabel.Anchor = _labelStyle;
            fifthPrize6DLabel.BackColor = Color.Transparent;

            Label first6DPrizeVal = new Label();
            first6DPrizeVal.Text = _poolResult.SixD;
            first6DPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            first6DPrizeVal.Anchor = _labelStyle;

            Label second6DPrizeVal = new Label();
            second6DPrizeVal.Text = _poolResult.SixD.Substring(0,5);
            second6DPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            second6DPrizeVal.Anchor = _labelStyle;

            Label third6DPrizeVal = new Label();
            third6DPrizeVal.Text = _poolResult.SixD.Substring(0, 4);
            third6DPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            third6DPrizeVal.Anchor = _labelStyle;

            Label fourth6DPrizeVal = new Label();
            fourth6DPrizeVal.Text = _poolResult.SixD.Substring(0, 3);
            fourth6DPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fourth6DPrizeVal.Anchor = _labelStyle;

            Label fifth6DPrizeVal = new Label();
            fifth6DPrizeVal.Text = _poolResult.SixD.Substring(0, 2);
            fifth6DPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fifth6DPrizeVal.Anchor = _labelStyle;

            Label second6DAltPrizeVal = new Label();
            second6DAltPrizeVal.Text = _poolResult.SixD.Substring(1, 5);
            second6DAltPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            second6DAltPrizeVal.Anchor = _labelStyle;

            Label third6DAltPrizeVal = new Label();
            third6DAltPrizeVal.Text = _poolResult.SixD.Substring(2, 4);
            third6DAltPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            third6DAltPrizeVal.Anchor = _labelStyle;

            Label fourth6DAltPrizeVal = new Label();
            fourth6DAltPrizeVal.Text = _poolResult.SixD.Substring(3, 3);
            fourth6DAltPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fourth6DAltPrizeVal.Anchor = _labelStyle;

            Label fifth6DAltPrizeVal = new Label();
            fifth6DAltPrizeVal.Text = _poolResult.SixD.Substring(4, 2);
            fifth6DAltPrizeVal.TextAlign = ContentAlignment.MiddleCenter;
            fifth6DAltPrizeVal.Anchor = _labelStyle;

            TableLayoutPanel table6D = new TableLayoutPanel();
            table6D.CellPaint += Table6DPaint;
            table6D.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            table6D.Dock = DockStyle.Fill;
            table6D.ColumnCount = 4;
            table6D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table6D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table6D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table6D.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            table6D.RowCount = 6;
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 16F));
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 16F));
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 16F));
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 16F));
            table6D.RowStyles.Add(new RowStyle(SizeType.Percent, 16F));

            Label title6D = new Label();
            title6D.Text = "6D";
            title6D.TextAlign = ContentAlignment.MiddleCenter;
            title6D.Anchor = _labelStyle;
            title6D.BackColor = Color.Transparent;

            table6D.Controls.Add(title6D);
            table6D.SetColumnSpan(title6D, 4);

            table6D.Controls.Add(firstPrize6DLabel, 0, 1);
            table6D.Controls.Add(secondPrize6DLabel, 0, 2);
            table6D.Controls.Add(thirdPrize6DLabel, 0, 3);
            table6D.Controls.Add(fourthPrize6DLabel, 0, 4);
            table6D.Controls.Add(fifthPrize6DLabel, 0, 5);
            
            table6D.Controls.Add(first6DPrizeVal, 1, 1);
            table6D.SetColumnSpan(first6DPrizeVal, 3);
            table6D.Controls.Add(second6DPrizeVal, 1, 2);
            table6D.Controls.Add(third6DPrizeVal, 1, 3);
            table6D.Controls.Add(fourth6DPrizeVal, 1, 4);
            table6D.Controls.Add(fifth6DPrizeVal, 1, 5);
            
            table6D.Controls.Add(second6DAltPrizeVal, 3, 2);
            table6D.Controls.Add(third6DAltPrizeVal, 3, 3);
            table6D.Controls.Add(fourth6DAltPrizeVal, 3, 4);
            table6D.Controls.Add(fifth6DAltPrizeVal, 3, 5);

            table6D.Controls.Add(orLabel, 2, 2);
            table6D.SetRowSpan(orLabel, 4);

            TableLayoutPanel _mainPanel = new TableLayoutPanel();
            _mainPanel.ColumnCount = 1;
            _mainPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            _mainPanel.RowCount = 2;
            _mainPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 40F));
            _mainPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 60F));
            _mainPanel.Dock = DockStyle.Fill;

            _mainPanel.Controls.Add(table5D);
            _mainPanel.Controls.Add(table6D);
            Controls.Add(_mainPanel);
        }

        private void Table5DPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row == 0)
            {
                e.Graphics.FillRectangle(Brushes.Green, e.CellBounds);
            }
            else if (e.Row > 0 && (e.Column == 0 || e.Column == 2))
            {
                e.Graphics.FillRectangle(Brushes.YellowGreen, e.CellBounds);
            }
        }

        private void Table6DPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row == 0)
            {
                e.Graphics.FillRectangle(Brushes.Green, e.CellBounds);
            }
            else if (e.Row > 0 && e.Column == 0)
            {
                e.Graphics.FillRectangle(Brushes.YellowGreen, e.CellBounds);
            }
        }
    }
}
