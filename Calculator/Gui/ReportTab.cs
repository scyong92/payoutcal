﻿using Calculator.Logic;
using Calculator.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using VStar.Backend.General.Utility;

namespace Calculator.Gui
{
    public class ReportTab : TabPage , ITab
    {
        private Calculator _cal;
        private TextBox _fileDicTxt = new TextBox();
        private FlowLayoutPanel _searchPanel = new FlowLayoutPanel();
        private TableLayoutPanel _resultTablePanel = new TableLayoutPanel();
        private Button _browseBtn = new Button();
        private Button _calcBtn = new Button();
        private Button _exportBtn = new Button();
        private List<PoolResult> _resulltModelList = new List<PoolResult>();
        private bool _isBusy = false;
        private Label _drawDateLabel = new Label();
        private OpenFileDialog _openFileDialog = new OpenFileDialog();
        private SaveFileDialog _saveFileDialog = new SaveFileDialog();

        private FlowLayoutPanel _drawDateSearchPanel = new FlowLayoutPanel();
        private FlowLayoutPanel _btnPanel = new FlowLayoutPanel();
        private DataGridView _reportPanel = new DataGridView();

        public ReportTab(Calculator cal)
        {
            _cal = cal;
            Init();
        }

        public void Init()
        {
            _fileDicTxt.Width = 200;
            _reportPanel.Dock = DockStyle.Fill;

            _drawDateLabel.Text = "Eat File :";
            _drawDateLabel.TextAlign = ContentAlignment.MiddleLeft;

            _drawDateSearchPanel.AutoSize = true;
            _drawDateSearchPanel.FlowDirection = FlowDirection.LeftToRight;
            _drawDateSearchPanel.Controls.Add(_drawDateLabel);
            _drawDateSearchPanel.Controls.Add(_fileDicTxt);
            _drawDateSearchPanel.Controls.Add(_browseBtn);

            _btnPanel.AutoSize = true;
            _btnPanel.FlowDirection = FlowDirection.LeftToRight;
            _btnPanel.Controls.Add(_calcBtn);
            _btnPanel.Controls.Add(_exportBtn);

            _browseBtn.Text = "Browse";
            _browseBtn.Click += BrowseBtnAction;

            _exportBtn.Text = "Export";
            _exportBtn.Click += ExportBtnAction;
            _exportBtn.Enabled = false;

            _calcBtn.Text = "Import";
            _calcBtn.Click += CalcBtnAction;

            _searchPanel.Dock = DockStyle.Top;
            _searchPanel.FlowDirection = FlowDirection.TopDown;
            _searchPanel.Padding = new Padding(10, 10, 10, 10);
            _searchPanel.Controls.Add(_drawDateSearchPanel);
            _searchPanel.Controls.Add(_btnPanel);

            Controls.Add(_reportPanel);
            Controls.Add(_searchPanel);
        }

        private void ExportBtnAction(object sender, EventArgs e)
        {
            _saveFileDialog.InitialDirectory = "C:\\";
            _saveFileDialog.Filter = "CSV (*.csv)|*.csv";
            if (_saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileDir = _saveFileDialog.FileName;

                string exportString = "";

                for (int i = 0; i < _reportPanel.Columns.Count; i++)
                {
                    exportString += _reportPanel.Columns[i].HeaderText + ",";
                }
                exportString = exportString.Substring(0, exportString.Length - 1);
                exportString += "\r\n";

                for (int j = 0; j < _reportPanel.RowCount; j++)
                {
                    for (int i = 0; i < _reportPanel.Columns.Count; i++)
                    {
                        exportString += _reportPanel.Rows[j].Cells[i].Value + ",";
                    }

                    exportString = exportString.Substring(0, exportString.Length - 1);
                    exportString += "\r\n";
                }

                System.IO.File.WriteAllText(fileDir, exportString);
                MessageBox.Show("File Saved");
            }
        }

        private void BrowseBtnAction(object sender, EventArgs e)
        {
            _openFileDialog.InitialDirectory = "C:\\";
            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _fileDicTxt.Text = _openFileDialog.FileName;
            }
        }

        private void CalcBtnAction(object sender, EventArgs e)
        {
            _isBusy = true;
            try
            {
                if (string.IsNullOrEmpty(_fileDicTxt.Text))
                {
                    throw new Exception("Please insert eat data file");
                }

                ResultLog result = EatDataReader.GetInstance().ReadAndCalculate(_fileDicTxt.Text);

                if (result.IsSuccess())
                {
                    List<PayoutModel> models = (List<PayoutModel>)result.ReturnObject;
                    _reportPanel.ColumnCount = 7;

                    _reportPanel.RowHeadersVisible = false;
                    _reportPanel.AllowUserToAddRows = false;
                    _reportPanel.AllowUserToDeleteRows = false;
                    _reportPanel.ReadOnly = true;
                    _reportPanel.EnableHeadersVisualStyles = false;
                    _reportPanel.ColumnHeadersDefaultCellStyle.BackColor = Color.YellowGreen;
                    _reportPanel.Columns[0].HeaderCell.Value = "Pool Name";
                    _reportPanel.Columns[1].HeaderCell.Value = "Bet Number";
                    _reportPanel.Columns[2].HeaderCell.Value = "Prize";
                    _reportPanel.Columns[3].HeaderCell.Value = "Eat Type";
                    _reportPanel.Columns[4].HeaderCell.Value = "Bet Amount";
                    _reportPanel.Columns[5].HeaderCell.Value = "Pay Rate";
                    _reportPanel.Columns[6].HeaderCell.Value = "Total Win";

                    _reportPanel.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    _reportPanel.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                    for (int i = 0; i < models.Count; i++)
                    {
                        _reportPanel.Rows.Add(models[i].PoolName, models[i].BetNumber, models[i].Prize, models[i].EatTypeName, models[i].BetAmount, models[i].Rate, models[i].Payout);
                    }

                    _reportPanel.ColumnHeadersDefaultCellStyle.Font = new Font(DataGridView.DefaultFont.FontFamily, 14F, FontStyle.Bold, GraphicsUnit.Pixel);

                    foreach (DataGridViewColumn c in _reportPanel.Columns)
                    {
                        c.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont.FontFamily, 16F, GraphicsUnit.Pixel);
                    }

                    _exportBtn.Enabled = models.Count > 0;
                    _reportPanel.ClearSelection();
                }
                else
                {
                    throw new Exception("Invalid File Format");
                }
            }
            catch (Exception ex)
            {
                _cal.HandleException(new CalcException(ex.Message));
            }
            finally
            {
                _isBusy = false;
            }
        }

        public bool IsReadyToClose()
        {
            if (_isBusy)
            {
                CalcException exception = new CalcException("Page is currently busy, please wait");
                _cal.HandleException(exception);
            }

            return !_isBusy;
        }

        public void Start()
        {
            //do nothing
        }

        public void Stop()
        {
            //do nothing
        }
    }
}
