﻿using Calculator.Logic;
using Calculator.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VStar.Backend.General.Utility;

namespace Calculator.Gui
{
    public class DrawResultTab : TabPage, ITab
    {
        private Calculator _cal;
        private DateTimePicker _resultTimePicker = new DateTimePicker();
        private FlowLayoutPanel _searchPanel = new FlowLayoutPanel();
        private TableLayoutPanel _resultTablePanel = new TableLayoutPanel();
        private Button _searchBtn = new Button();
        private BackgroundWorker _backgroundWorker;
        private List<PoolResult> _resulltModelList= new List<PoolResult>();
        private bool _isBusy = false;
        private TabControl _poolTab = new TabControl();
        private Label _drawDateLabel = new Label();

        private FlowLayoutPanel _drawDateSearchPanel = new FlowLayoutPanel();

        public DrawResultTab(Calculator cal)
        {
            _cal = cal;
            Init();
        }

        public void Init()
        {
            _drawDateLabel.Text = "Draw Date";
            _drawDateLabel.TextAlign = ContentAlignment.MiddleLeft;

            _drawDateSearchPanel.AutoSize = true;
            _drawDateSearchPanel.FlowDirection = FlowDirection.LeftToRight;
            _drawDateSearchPanel.Controls.Add(_drawDateLabel);
            _drawDateSearchPanel.Controls.Add(_resultTimePicker);
            
            _searchBtn.Text = "Search";
            _searchBtn.Click += SearchBtnAction;

            _searchPanel.Dock = DockStyle.Top;
            _searchPanel.FlowDirection = FlowDirection.TopDown;
            _searchPanel.Padding = new Padding(10, 10, 10, 10);
            _searchPanel.Controls.Add(_drawDateSearchPanel);
            _searchPanel.Controls.Add(_searchBtn);

            _poolTab.Dock = DockStyle.Fill;

            Controls.Add(_poolTab);
            Controls.Add(_searchPanel);
        }

        private void SearchBtnAction(object sender, EventArgs e)
        {
            _isBusy = true;
            _searchBtn.Enabled = false;
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(PerformResultFetch);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ResultPoolComplete);
            _backgroundWorker.RunWorkerAsync(PoolResultReader.GetInstance().CheckResult(_resultTimePicker.Value.ToString("dd/MM/yyyy")));
        }

        private void ResultPoolComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            _isBusy = false;
            _searchBtn.Enabled = true;

            if (_resulltModelList == null || _resulltModelList.Count == 0)
            {
                MessageBox.Show("No Draw Result", "Draw Result");
            }

            _poolTab.Controls.Clear();
            foreach (var ea in _resulltModelList)
            {
                TabPage poolTab = new PoolResultPanel4D(ea);
                poolTab.Text = ea.Pool.PoolName;
                _poolTab.Controls.Add(poolTab);

                if (ea.Pool.Has5D || ea.Pool.Has6D)
                {
                    TabPage pool5D6DTab = new PoolResultPanel5D6D(ea);
                    pool5D6DTab.Text = ea.Pool.PoolName+ "(5D/6D)";
                    _poolTab.Controls.Add(pool5D6DTab);
                }
            }
        }

        private void PerformResultFetch(object sender, DoWorkEventArgs e)
        {
            ResultLog result = PoolResultReader.GetInstance().CheckResult(_resultTimePicker.Value.ToString("dd/MM/yyyy"));
            if (result.IsSuccess())
            {
                _resulltModelList.Clear();
                _resulltModelList = (List<PoolResult>)result.ReturnObject;
            }
        }

        public bool IsReadyToClose()
        {
            if (_resulltModelList == null || _resulltModelList.Count == 0)
            {
                CalcException exception = new CalcException("Please search draw result first");
                _cal.HandleException(exception);
            }

            if (_isBusy)
            {
                CalcException exception = new CalcException("Page is currently busy, please wait");
                _cal.HandleException(exception);
            }

            return !_isBusy && _resulltModelList != null && _resulltModelList.Count > 0;
        }

        public void Start()
        {
            //do nothing
        }

        public void Stop()
        {
            //do nothing
        }
    }
}
