﻿using Calculator.Gui;
using Calculator.Logic;
using Calculator.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VStar.Backend.General.Utility;

namespace Calculator
{
    public class Calculator : Form
    {
        private TabControl _mainTab = new TabControl();
        private TabPage _resultTab;
        private TabPage _reportTab;

        public Calculator()
        {
            _resultTab = new DrawResultTab(this);
            _reportTab = new ReportTab(this);
            Init();
        }

        private void Init()
        {
            _resultTab.Text = "Result";
            _reportTab.Text = "Report";
            _mainTab.Dock = DockStyle.Fill;
            _mainTab.Selecting += TabChange;
            _mainTab.Deselecting += TabDeselecting;

            _mainTab.TabPages.Add(_resultTab);
            _mainTab.TabPages.Add(_reportTab);
            this.Size = new Size(640, 480);
            Controls.Add(_mainTab);

            PoolResultReader.GetInstance().ReadConfigPool();
        }

        public void TabChange(object sender, TabControlCancelEventArgs e)
        {
            ITab currentTab = (ITab)(sender as TabControl).SelectedTab;
            currentTab.Start();
        }

        public void TabDeselecting(object sender, TabControlCancelEventArgs e)
        {
            ITab currentTab = (ITab)(sender as TabControl).SelectedTab;

            bool readyToClose = currentTab.IsReadyToClose();
            if (readyToClose)
            {
                currentTab.Stop();
            }

            e.Cancel = !readyToClose;
        }

        public void HandleException(CalcException ex)
        {
            MessageBox.Show(ex.Message, "Error");
        }
    }
}
