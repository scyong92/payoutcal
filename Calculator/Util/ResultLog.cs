﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VStar.Backend.General.Utility
{
    public class ResultLog
    {
        public int Code { get; set; }

        public int Count { get; set; }

        public string Message { get; set; }

        public string ClassName { get; set; }

        public string TagName { get; set; }

        public object ReturnObject { get; set; }

        public Dictionary<string, object> Responses { get; set; }

        public ResultLog()
        {
            Responses = new Dictionary<string, object>();
        }

        public ResultLog(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public bool IsSuccess()
        {
            return Code == 0;
        }
    }
}
