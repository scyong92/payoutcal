﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Util
{
    public class CalcException : Exception
    {
        public CalcException(String message)
        :base(message)
        {
            
        }
    }
}
